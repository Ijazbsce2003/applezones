﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Apple.web.Startup))]
namespace Apple.web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
